var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	userid: String,
	name: String,
	firstName: String,
	email: String,
	passwordHash: String,
	passwordSalt: String,
	lastLocation: { type : { type: String, default: 'Point' } , coordinates: [Number] }
},
{collection : "users"});

var User = mongoose.model('User', UserSchema);

module.exports = User;