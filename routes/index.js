var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Surveyor' });
});

router.get('/main', function(req, res) {
	res.render('main', { title: 'Dashboard'});
});

router.get('/profile', function(req, res) {
	res.render('profile', { title: 'Profile page'});
});

router.get('/record', function(req, res) {
	res.render('record', { title: 'Set up new report'});
});

router.get('/record_data', function(req, res) {
    res.render('record_data', { title: 'Recording data'});
});

router.get('/discover', function(req, res) {
	res.render('discover', { title: 'Discover'});
});

module.exports = router;
