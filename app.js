var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var mongoose = require('mongoose');

var app = express();

  console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++");
  console.log("BULLSHIT ERROR REPORTING ENDS RIGHT FUCKING HERE.");
  console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++");

var db = mongoose.connect('mongodb://localhost/Project2');
var dbConn = mongoose.connection;

dbConn.on('error', function(msg) {
  console.log("-- DB Conn failed (apps.js) --");
});

dbConn.once('open', function() {
  console.log("-- DB Conn success (apps.js) --");
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use('/', routes);
app.use('/main', routes);
app.use('/profile', routes);
app.use('/record', routes);
app.use('/discover', routes);


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
