//Location function
$(document).ready(function() {
    console.log("Locator loaded");
    $("#map").hide().slideDown();
    // saveLocation();

});

// MAP OP RECORD
var map;


function saveLocation() {

    if (!navigator.geolocation) return;

    navigator.geolocation.getCurrentPosition(function(position) {

        geocoder = new google.maps.Geocoder();

        var GeoSucces = 0;
        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        // 49.2496600, -123.1193400
        // position.coords.latitude, position.coords.longitude

        var mapOptions = {
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
        };

        var map = new google.maps.Map(document.getElementById('map'),
        mapOptions);

        map.setCenter(latlng);

        geocoder.geocode({
            'latLng': latlng
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log("Geocoding success");
                console.log(results);
                codedAddress = results[0].formatted_address;
                GeoSucces = 1;
            }
            console.log(codedAddress);

            var loc_Country = "";

            if (results[8] !== undefined && results[8].address_components[0].long_name !== undefined) {
                var loc_Country = results[8].address_components[0].long_name
                var loc_Street = results[0].address_components[1].long_name;
                var loc_StNumber = results[0].address_components[0].long_name;
                var loc_City = results[0].address_components[2].long_name;
                var loc_Province = results[0].address_components[3].long_name;
                var loc_Region = results[0].address_components[4].long_name;
                document.getElementById("locationField").innerHTML = loc_Street + " " + loc_StNumber + ", " + loc_City + " (" + loc_Province + "), " + loc_Country;
                $(".TweetLink").attr("href", "https://twitter.com/intent/tweet?text=Making sure I never forget my time near " + loc_Street + " in " + loc_City + "! Check out &hashtags=,surveyor, &via=surveyorapp!");
                $(".location-field-profile").text(loc_City);
                return;
            } else {
                var loc_Country = results[5].address_components[0].long_name
                var loc_Street = results[0].address_components[1].long_name;
                var loc_StNumber = results[0].address_components[0].long_name;
                var loc_City = results[0].address_components[2].long_name;
                var loc_Province = results[0].address_components[3].long_name;
                var loc_Region = results[0].address_components[4].long_name;
                document.getElementById("locationField").innerHTML = loc_Street + " " + loc_StNumber + ", " + loc_City + " (" + loc_Province + "), " + loc_Country;
                $(".TweetLink").attr("href", "https://twitter.com/intent/tweet?text=Making sure I never forget my time near " + loc_Street + " in " + loc_City + "! Check out &hashtags=,surveyor, &via=surveyorapp!");
                $(".location-field-profile").text(" "+loc_City);
                return;
            }

            map.setCenter(latlng);

        });

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Your approximate location'
        });
    });
};

function LocationWatcher() {
    var self = this;
    var map;

    self.watcherId = undefined;
    self.location = {
        long: 0,
        lat: 0
    };

    self.startWatching = startWatching;
    self.stopWatching = stopWatching;
    self.foundLocation = foundLocation;
    self.locationError = locationError;


    function startWatching() {
        self.watcherId = navigator.geolocation.watchPosition(self.foundLocation, self.locationError, {
            timeout: 60000
        });
    }

    function stopWatching() {
        navigator.geolocation.clearWatch(self.watcherId);
    }

    function foundLocation(position) {
        console.log('found your location: ', position)

        geocoder = new google.maps.Geocoder();

        var latlng2 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        // 49.2496600, -123.1193400
        // position.coords.latitude, position.coords.longitude
        var GeoSucces = 0;

        var mapOptions = {
            zoom: 14,
            center: latlng2,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };

        var map = new google.maps.Map(document.getElementById('map'),
            mapOptions);

        map.setCenter(latlng2);

        var marker = new google.maps.Marker({
            position: latlng2,
            map: map,
            title: 'Your approximate location'
        });

        geocoder.geocode({
            'latLng': latlng2
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log("WatchPosition geocoding success");
                console.log(results);
                map.setCenter(latlng2);
                codedAddress = results[0].formatted_address;
                GeoSucces = 1;
            }
            console.log(codedAddress);

            var loc_Country = "";

            if (results[8] !== undefined && results[8].address_components[0].long_name !== undefined) {
                var loc_Country = results[8].address_components[0].long_name
                var loc_Street = results[0].address_components[1].long_name;
                var loc_StNumber = results[0].address_components[0].long_name;
                var loc_City = results[0].address_components[2].long_name;
                var loc_Province = results[0].address_components[3].long_name;
                var loc_Region = results[0].address_components[4].long_name;
                $("#locationTicker").append("<li style='height: 1em;'>" + loc_Street + " " + loc_StNumber + ", " + loc_City + " (" + loc_Province + "), " + loc_Country + "</li><hr>").slideDown();
                console.log('appendli lol');
                return;
            } else {
                var loc_Country = results[5].address_components[0].long_name
                var loc_Street = results[0].address_components[1].long_name;
                var loc_StNumber = results[0].address_components[0].long_name;
                var loc_City = results[0].address_components[2].long_name;
                var loc_Province = results[0].address_components[3].long_name;
                var loc_Region = results[0].address_components[4].long_name;
                $("#locationTicker").slideDown().append("<li style='height: 25px;'>" + loc_Street + " " + loc_StNumber + ", " + loc_City + " (" + loc_Province + "), " + loc_Country + "</li><hr>").slideDown();
                console.log('appendli lol');
                return;
            }
            map.setCenter(latlng2);
            
        });

        $("#stopWatching").on("click", function(){ 
            $("#spinner").css("display", "none");
            $(".status").html("Recorded results:");
            $("#backButton").removeClass("disabled");
            $("#stopWatching").addClass("disabled");

            stopWatching();

    });

    }

    function locationError(error) {
        console.log('error finding location: ', error)
        self.stopWatching();
    }

}