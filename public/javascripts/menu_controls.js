$(document).ready(function() {
    // SIDR SIDE MENU
    $(".toggle").sidr();

    //HELP BOXES
    var enabledState = 0;

    $("#mapSeparator").on("click", function() {
        if (enabledState == 0){
            $("#map").slideUp("fast");
            $("#chevronUp").hide();
            $("#chevronDown").show();
            enabledState = 1
            return;
        }
        else {
            $("#map").slideDown("fast");
            $("#chevronUp").show();
            $("#chevronDown").hide();
            enabledState = 0
            return;
        }
    });

    $("#controlsHelp1").on("click", function() {
        if (enabledState == 0) {
            $("#infoBox1").slideDown("fast");
            enabledState = 1
            return;
        } else {
            $("#infoBox1").slideUp();
            enabledState = 0
            return;
        }
    });

    $("#controlsHelp2").on("click", function() {
        if (enabledState == 0) {
            $("#infoBox2").slideDown("fast");
            enabledState = 1
            return;
        } else {
            $("#infoBox2").slideUp();
            enabledState = 0
            return;
        }
    });

    $("#controlsHelp3").on("click", function() {
        if (enabledState == 0) {
            $("#infoBox3").slideDown("fast");
            enabledState = 1
            return;
        } else {
            $("#infoBox3").slideUp();
            enabledState = 0
            return;
        }
    });

    $("#controlsHelp4").on("click", function() {
        if (enabledState == 0) {
            $("#infoBox4").slideDown("fast");
            enabledState = 1
            return;
        } else {
            $("#infoBox4").slideUp();
            enabledState = 0
            return;
        }
    });

	$("#controlsHelp5").on("click", function() {
	        if (enabledState == 0) {
	            $("#infoBox5").slideDown("fast");
	            enabledState = 1
	            return;
	        } else {
	            $("#infoBox5").slideUp();
	            enabledState = 0
	            return;
	        }
	    });

	$("#controlsHelp6").on("click", function() {
		        if (enabledState == 0) {
		            $("#infoBox6").slideDown("fast");
		            enabledState = 1
		            return;
		        } else {
		            $("#infoBox6").slideUp();
		            enabledState = 0
		            return;
		        }
		    });


	$("#mapSeparator").hide().slideDown("slow").show();
	$("#locationField").hide().slideDown("slow").show();
    $("#dashboardContent").hide().slideDown("slow").show();
    $("#dashboardContent img").hide().slideDown("slow").show();
    $("#dashboardContent h3").hide().slideDown("slow").show();
    $("#dashboardContent2").hide().slideDown("slow").show();
    $("#dashboardContent2 img").hide().slideDown("slow").show();
    $("#dashboardContent2 h3").hide().slideDown("slow").show();


});